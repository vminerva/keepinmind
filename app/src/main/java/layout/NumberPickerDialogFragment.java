package layout;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.common.Constants;

public class NumberPickerDialogFragment extends DialogFragment {

    private NumberPicker ciclo;
    private Button btnCancelar, btnOk;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public NumberPickerDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_number_picker_dialog, container, false);
        initComponents(view);

        getDialog().setTitle("Duração do Ciclo");
        ciclo.setMinValue(1);
        ciclo.setMaxValue(28);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt(Constants.CICLO, ciclo.getValue());
                editor.commit();
                getDialog().dismiss();
            }
        });

        return view;
    }

    public void initComponents(View view){
        ciclo       = (NumberPicker) view.findViewById(R.id.valor_number);
        btnCancelar = (Button) view.findViewById(R.id.btnBack);
        btnCancelar.setText("CANCELAR");
        btnOk       = (Button) view.findViewById(R.id.btnNext);
        btnOk.setText("OK");
        preferences         = getActivity().getSharedPreferences(Constants.DADOS, Context.MODE_PRIVATE);
        editor              = preferences.edit();
    }
}
