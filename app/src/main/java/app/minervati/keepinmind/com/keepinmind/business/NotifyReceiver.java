package app.minervati.keepinmind.com.keepinmind.business;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.activities.BaseAlertaActivity;
import app.minervati.keepinmind.com.keepinmind.activities.HomeActivity;

public class NotifyReceiver extends BroadcastReceiver {

    int MID = 0;

    @Override
    public void onReceive(Context context, Intent intent) {

        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent = new Intent(context, BaseAlertaActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        android.support.v4.app.NotificationCompat.Builder mNotifyBuilder = new android.support.v4.app.NotificationCompat.Builder(
                context).setSmallIcon(R.mipmap.ic_pill)
                .setTicker("Alerta diário")
                .setContentTitle("Hora do anticomcepcional")
                .setContentText("Lembre a sua parceira!").setSound(alarmSound)
                .setAutoCancel(true).setWhen(when)
                .setContentIntent(pendingIntent)
                .setVibrate(new long[]{150, 1000, 150, 1000});

        notificationManager.notify(MID, mNotifyBuilder.build());

        MID++;
    }
}