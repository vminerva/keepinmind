package app.minervati.keepinmind.com.keepinmind.fragments;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateChangedListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.business.CalendarioBusiness;
import app.minervati.keepinmind.com.keepinmind.common.Constants;
import app.minervati.keepinmind.com.keepinmind.common.EventDecoratorDay;
import app.minervati.keepinmind.com.keepinmind.common.OneDayDecorator;


public class OneFragment extends Fragment {

    private OneDayDecorator oneDayDecorator;
    private MaterialCalendarView calendarView;
    private Calendar calendario = Calendar.getInstance();
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private FragmentTransaction fragTran;
    private DateFormat dateFormat;

    public OneFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_one, container, false);
        initComponent(layout);

        defineLayoutCalendarView();

        calendarView.setOnDateChangedListener(new OnDateChangedListener() {
            @Override
            public void onDateChanged(@NonNull MaterialCalendarView materialCalendarView, @Nullable CalendarDay calendarDay) {
                oneDayDecorator.setDate(calendarDay.getDate());
                calendarView.invalidateDecorators();
            }
        });

        calendarView.addDecorators(oneDayDecorator);

        new ApiSimulator().executeOnExecutor(Executors.newSingleThreadExecutor());

        return layout;
    }

    private void defineLayoutCalendarView() {
        calendarView.setHeaderTextAppearance(R.style.TextAppearance_AppCompat_Large);
        calendarView.setDateTextAppearance(R.style.TextAppearance_AppCompat_Light_Widget_PopupMenu_Large);
        calendarView.setWeekDayTextAppearance(R.style.TextAppearance_AppCompat_Medium);
        calendarView.setSelectionColor(getResources().getColor(R.color.colorPrimary));
        calendarView.setTileSizeDp(50);
        calendarView.setShowOtherDates(true);
        calendarView.setSelectedDate(calendario.getTime());

    }

    private void initComponent(View layout){
        oneDayDecorator = new OneDayDecorator();
        calendarView    = (MaterialCalendarView) layout.findViewById(R.id.calendar);
        preferences     = getActivity().getSharedPreferences(Constants.DADOS, Context.MODE_PRIVATE);
        editor          = preferences.edit();
        fragTran        = getActivity().getFragmentManager().beginTransaction();
        dateFormat      = new SimpleDateFormat("dd/MM/yyyy");
    }

    /**
     * Simular uma chamada de API para mostrar como adicionar decoradores
     */
    private class ApiSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {

        private CalendarioBusiness calendarioBusiness;
        private Calendar calendar = Calendar.getInstance();
        private Integer dia;
        private Integer mes;
        private Integer ano;

        @Override
        protected List<CalendarDay> doInBackground(Void... voids) {
            String date = preferences.getString(Constants.DATA, "");
            String[] dataSalva = obtemDataSalvaInString(date);

            if (dataSalva != null) {
                if (!"".equals(dataSalva[0])){
                    mes = Integer.parseInt(dataSalva[0]);
                    dia = Integer.parseInt(dataSalva[1]);
                    ano = Integer.parseInt(dataSalva[2]);
                    calendar.set(ano, mes-1, dia);
                }
            }

            ArrayList<CalendarDay> dates = new ArrayList<>();

            //Data que iniciou o comprimido. Caso não haja data salva, ele pega a data do sistema
            CalendarDay diaMenstrual = CalendarDay.from(calendar);
            dates.add(diaMenstrual);
            calendar.add(Calendar.DATE, 1);

            return dates;
        }

        @Override
        protected void onPostExecute(List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);

            ArrayList<CalendarDay> dates = new ArrayList<>();

            if (getActivity().isFinishing()) {
                return;
            }

            //DIA DO INICIO DA PILULA, INFORMADO PELO USUARIO
            CalendarDay diaMenstruacao = calendarDays.get(0);
            calendarView.addDecorator(new EventDecoratorDay(Color.RED, diaMenstruacao));

            //PEGA O DIA INFORMADO NA DEFINIÇÃO E CALCULA O PROXIMO DIA DA MESNTRUAÇÃO
            Calendar calendarMenstruacao = Calendar.getInstance();
            calendar = Calendar.getInstance();
            calendar.setTime(diaMenstruacao.getDate());

            Integer ciclo = preferences.getInt(Constants.CICLO, 0);
            Integer cicloMens = preferences.getInt(Constants.CICLOMENSTRUAL, 0);
            int cicloObtido = 0;
            int cicloMensObtido =0;
            if (!"".equals(ciclo)) {
                cicloObtido = ciclo - 1;
                cicloMensObtido = cicloMens + 1;
            }

            for (int i=0; i < ciclo-11; i++) {
                //Informa o ultimo dia do comprimido
                calendar.add(Calendar.DATE, +cicloObtido);
                CalendarDay ultmDiaComprimido = CalendarDay.from(calendar);
                calendarView.addDecorator(new EventDecoratorDay(Color.parseColor("#0000EE"), ultmDiaComprimido));

                String dataFimComp = dateFormat.format(ultmDiaComprimido.getDate());
                //Log.i(Constants.DATA_FIM_COMP, dataFimComp);

                //Calcula os dias ferteis e pega o ultimo dia fertil
                CalendarDay ultimoDiaFertil = getCalendarioBusiness().diasFerteis(calendarDays, dates, calendarView);

                //Calcula os dias inferteis
                CalendarDay ultimoDiaInfertil = getCalendarioBusiness().diasInferteis(ultimoDiaFertil, cicloObtido, calendarView);

                //Informa o dia para retomar o comprimido
                calendarMenstruacao.setTime(ultmDiaComprimido.getDate());
                calendarMenstruacao.add(Calendar.DATE, +(cicloMensObtido - 1));
                CalendarDay proxDiaComprimido = CalendarDay.from(calendarMenstruacao);
                calendarView.addDecorator(new EventDecoratorDay(Color.RED, proxDiaComprimido));

                String dataProxDiaComp = dateFormat.format(proxDiaComprimido.getDate());
                //Log.i(Constants.DATA_PROX_COMP, dataProxDiaComp);

                getCalendarioBusiness().diasTPM(ultimoDiaInfertil, calendarView);

                calendar.setTime(ultmDiaComprimido.getDate());
                calendarDays.clear();
                calendarDays.add(ultmDiaComprimido);
                calendarDays.clear();
                calendar.setTime(proxDiaComprimido.getDate());
                calendarDays.add(proxDiaComprimido);
            }
        }

        /*
         * Singleton no negocio: calendarioBusiness
         */
        public CalendarioBusiness getCalendarioBusiness() {
            if (calendarioBusiness == null) {
                calendarioBusiness = new CalendarioBusiness();
            }
            return calendarioBusiness;
        }

        public String[] obtemDataSalvaInString(String date) {
            String[] dateValues = null;
            if (date != null){
                dateValues = date.split("/");
            }
            return dateValues;
        }
    }
}