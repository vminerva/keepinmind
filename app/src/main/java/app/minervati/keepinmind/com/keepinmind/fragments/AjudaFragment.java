package app.minervati.keepinmind.com.keepinmind.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.activities.TemplateActivity;
import app.minervati.keepinmind.com.keepinmind.common.Constants;
import app.minervati.keepinmind.com.keepinmind.common.CustomListAdapter;

public class AjudaFragment extends Fragment {

    private ListView listaItensConfig;
    private List<String> itensAjuda = new ArrayList<String>();
    private List<Integer> images = new ArrayList<Integer>();

    {
        itensAjuda.add("Sobre");
        itensAjuda.add("FAQ");
        itensAjuda.add("Fale Conosco ");

        images.add(R.drawable.mcv_action_next);
        images.add(R.drawable.mcv_action_next);
        images.add(R.drawable.mcv_action_next);
    }

    public AjudaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ajuda, container, false);
        initComponent(view);

        CustomListAdapter adapter = new CustomListAdapter(getActivity(), itensAjuda, null);
        listaItensConfig.setAdapter(adapter);

        listaItensConfig.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        //Intent i_help = new Intent(getActivity(), TemplateActivity.class);
                        //i_help.putExtra(Constants.POSICAO, position);
                        break;
                    case 1:
                        //Intent i_def = new Intent(getActivity(), TemplateActivity.class);
                        //i_def.putExtra(Constants.POSICAO, position);
                        //startActivity(i_def);
                        break;
                    case 2:
                        //Intent i_not = new Intent(getActivity(), TemplateActivity.class);
                        //i_not.putExtra(Constants.POSICAO, position);
                        break;
                }
            }
        });

        return view;
    }

    private void initComponent(View view){
        listaItensConfig    = (ListView) view.findViewById(R.id.list_ajuda);
    }

}
