package app.minervati.keepinmind.com.keepinmind.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.NumberPicker;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.common.AndroidUtil;
import app.minervati.keepinmind.com.keepinmind.common.Constants;

public class CicloFragment extends Fragment {

    private Button                      btnProximo;
    private Button                      btnAnterior;
    private FragmentManager             fragManager;
    private FragmentTransaction         fragmentTransaction;
    private ImageButton                 telaDadosTres;
    private NumberPicker                npCiclo;
    private Integer                     cicloMenstrual;

    public CicloFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ciclo, container, false);
        initComponents(view);

        telaDadosTres.setPressed(Boolean.TRUE);

        npCiclo.setMinValue(21);
        npCiclo.setMaxValue(28);

        //PEGA O VALOR DO CICLO CASO EXISTA SALVO
        cicloMenstrual = (Integer) AndroidUtil.getMap().get(Constants.CICLO);
        if(!AndroidUtil.isNull(cicloMenstrual))
            npCiclo.setValue(cicloMenstrual);

        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction.setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out);
                Integer ciclo = npCiclo.getValue();
                AndroidUtil.setMap(Constants.CICLO, ciclo);
                DuracaoMenstruacaoFragment duracaoMenstruacaoFragment = new DuracaoMenstruacaoFragment();
                fragmentTransaction.replace(R.id.container, duracaoMenstruacaoFragment);
                fragmentTransaction.commit();
            }
        });

        btnAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction.setCustomAnimations(R.animator.card_flip_left_in, R.animator.card_flip_left_out,
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out);
                DataPilulaFragment dataPilulaFragment = new DataPilulaFragment();
                fragmentTransaction.replace(R.id.container, dataPilulaFragment);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    public void initComponents(View view){
        btnProximo          = (Button) view.findViewById(R.id.btnNext);
        btnAnterior         = (Button) view.findViewById(R.id.btnBack);
        fragManager         = getFragmentManager();
        fragmentTransaction = fragManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        telaDadosTres       = (ImageButton) view.findViewById(R.id.tela_dados_tres);
        npCiclo = (NumberPicker) view.findViewById(R.id.np_ciclo);
    }

}
