package app.minervati.keepinmind.com.keepinmind.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.activities.HomeActivity;
import app.minervati.keepinmind.com.keepinmind.common.AndroidUtil;
import app.minervati.keepinmind.com.keepinmind.common.Constants;

public class RevisaoFragment extends Fragment {

    private Button btnConcluir;
    private Button btnAnterior;
    private FragmentManager fragManager;
    private FragmentTransaction fragmentTransaction;
    private ImageButton telaDadosCinco;
    private TextView valueData;
    private TextView valueCiclo;
    private TextView valueCicloMenst;

    private String dataSalva;
    private Integer ciclo;
    private Integer cicloMenst;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public RevisaoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_revisao, container, false);
        initComponents(view);

        telaDadosCinco.setPressed(Boolean.TRUE);

        //RECUPERA TODOS OS VALORES SALVOS PARA CONFIRMAR COM O USUARIO
        preencheValores();

        btnConcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String data = (String) AndroidUtil.getMap().get(Constants.DATA);
                editor.putString(Constants.DATA, data);
                editor.putInt(Constants.CICLO, ciclo);
                editor.putInt(Constants.CICLOMENSTRUAL, cicloMenst);
                editor.commit();
                Intent i = new Intent(getActivity(), HomeActivity.class);
                startActivity(i);
                // close this activity
                getActivity().finish();
            }
        });

        btnAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction.setCustomAnimations(R.animator.card_flip_left_in, R.animator.card_flip_left_out,
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out);
                DuracaoMenstruacaoFragment duracaoMenstruacaoFragment = new DuracaoMenstruacaoFragment();
                fragmentTransaction.replace(R.id.container, duracaoMenstruacaoFragment);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    private void preencheValores() {
        dataSalva = formatDate();
        valueData.setText(dataSalva);
        ciclo = (Integer) AndroidUtil.getMap().get(Constants.CICLO);
        valueCiclo.setText(String.valueOf(ciclo));
        cicloMenst = (Integer) AndroidUtil.getMap().get(Constants.CICLOMENSTRUAL);
        valueCicloMenst.setText(String.valueOf(cicloMenst));
    }

    public void initComponents(View view) {
        btnConcluir = (Button) view.findViewById(R.id.btnNext);
        alterStyleBotao();
        btnAnterior = (Button) view.findViewById(R.id.btnBack);
        fragManager = getFragmentManager();
        fragmentTransaction = fragManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        telaDadosCinco = (ImageButton) view.findViewById(R.id.tela_dados_cinco);
        valueData = (TextView) view.findViewById(R.id.valueData);
        valueCiclo = (TextView) view.findViewById(R.id.valueCiclo);
        valueCicloMenst = (TextView) view.findViewById(R.id.valueMens);
        preferences         = getActivity().getSharedPreferences(Constants.DADOS, Context.MODE_PRIVATE);
        editor              = preferences.edit();
    }

    private void alterStyleBotao() {
        btnConcluir.setText("CONCLUIR");
        btnConcluir.setBackgroundColor(Color.RED);
        btnConcluir.setTextColor(Color.WHITE);
    }

    public String formatDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy");
        Calendar calendar = Calendar.getInstance();
        Integer ano, mes, dia;
        String data = (String) AndroidUtil.getMap().get(Constants.DATA);
        Log.i(Constants.DATA, data);
        String[] dataGravada = AndroidUtil.obtemDataSalvaInString(data);
        if (dataGravada != null) {
            if (!"".equals(dataGravada[0])) {
                calendar.set(Integer.parseInt(dataGravada[2]),
                        Integer.parseInt(dataGravada[1]) - 1,
                        Integer.parseInt(dataGravada[0]));
            }
        }

        String d = sdf.format(calendar.getTime());
        Log.i(Constants.DATA, d);
        return d;
    }


}
