package app.minervati.keepinmind.com.keepinmind.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.common.AndroidUtil;
import app.minervati.keepinmind.com.keepinmind.common.Constants;

public class SplashScreenActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private SharedPreferences preferences;
    private String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_activity);

        preferences = getSharedPreferences(Constants.DADOS, Context.MODE_PRIVATE);
        data = preferences.getString(Constants.DATA, "");

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if (AndroidUtil.isNullOrEmpty(data)){
                    Intent i = new Intent(SplashScreenActivity.this, BaseActivity.class); //InfoBemVindoActivity.class
                    startActivity(i);
                }else{
                    Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);
                    startActivity(i);
                }

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
