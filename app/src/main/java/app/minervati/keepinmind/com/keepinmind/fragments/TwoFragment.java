package app.minervati.keepinmind.com.keepinmind.fragments;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.common.Constants;

public class TwoFragment extends Fragment implements TimePickerDialog.OnTimeSetListener {

    private Calendar calendar = Calendar.getInstance();
    private LinearLayout container;
    private TextView clock;
    private TimePickerDialog timePicker;
    private ToggleButton lembreteHora;
    private Integer hour, minute;
    private FloatingActionButton fab;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private PendingIntent pendingIntent;

    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_two, container, false);
        initComponents(view);

        final String horario = preferences.getString(Constants.HORARIO, "");
        boolean isHoraAtiva = preferences.getBoolean(Constants.HORARIO_ATIVO, false);

        checkTextViewForSetValues(horario, clock, this.lembreteHora, isHoraAtiva);

        clock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horarioDialog().show(getActivity().getFragmentManager(), "timePicker");
            }
        });

        lembreteHora.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    clock.setEnabled(true);
                    horarioDialog().show(getActivity().getFragmentManager(), "timePicker");
                } else {
                    clock.setEnabled(false);
                }
                editor.putBoolean(Constants.HORARIO_ATIVO, isChecked);
                editor.commit();
            }
        });

        return view;
    }

    public void initComponents(View view) {
        clock = (TextView) view.findViewById(R.id.relogio);
        clock.setText(calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE));
        lembreteHora = (ToggleButton) view.findViewById(R.id.swtLembrete);
        //container   = (LinearLayout) view.findViewById(R.id.container_two);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        preferences = getActivity().getSharedPreferences(Constants.DADOS, Context.MODE_PRIVATE);
        editor = preferences.edit();
        // Create a new service client and bind our activity to this service
    }


    /*
    private void addItem() {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View addView = layoutInflater.inflate(R.layout.content_row, null);

        TextView textOut = (TextView)addView.findViewById(R.id.textout);
        textOut.setText("VICTOR TESTE");

        Button buttonRemove = (Button)addView.findViewById(R.id.remove);
        buttonRemove.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ((LinearLayout)addView.getParent()).removeView(addView);
            }
        });
        container.addView(addView);
    }
    */

    private void initTime() {
        hour = calendar.get(Calendar.HOUR);
        minute = calendar.get(Calendar.MINUTE);
    }

    private TimePickerDialog horarioDialog() {
        initTime();

        Calendar cDefault = Calendar.getInstance();
        cDefault.set(0, 0, 0, hour, minute);
        timePicker = TimePickerDialog.newInstance(
                this,
                cDefault.get(Calendar.HOUR_OF_DAY),
                cDefault.get(Calendar.MINUTE),
                false);

        timePicker.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                lembreteHora.setChecked(false);
            }
        });

        return timePicker;
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        this.hour = hourOfDay;
        this.minute = minute;

        String horario = (hour < 10 ? "0" + hour : hour) + ":"
                + (minute < 10 ? "0" + minute : minute);

        clock.setText(horario);

        getActivity().recreate();

        editor.putString(Constants.HORARIO, horario);
        Log.i(Constants.HORARIO, horario);
        editor.commit();

    }

    private void checkTextViewForSetValues(String string, TextView txt, ToggleButton btn, Boolean b) {
        if (!"".equals(string)) {
            txt.setText(string);
            txt.setEnabled(b);
            btn.setChecked(b);
        }
    }


    /*
    private void openAgendaContatos() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE); // Mostra apenas contatos com numeros de celular
        startActivityForResult(pickContactIntent, Constants.PICK_CONTACT_REQUEST);
    }
    */
}