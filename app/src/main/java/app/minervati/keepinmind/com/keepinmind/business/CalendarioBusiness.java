package app.minervati.keepinmind.com.keepinmind.business;

import android.graphics.Color;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.minervati.keepinmind.com.keepinmind.common.EventDecorator;
import app.minervati.keepinmind.com.keepinmind.domain.Calendario;

/**
 * Created by victor minerva on 04/07/2015.
 */
public class CalendarioBusiness {

    Calendario calendario = new Calendario();

    public CalendarioBusiness() {}

    public Calendar calculaPeriodoFertil() {
        return Calendar.getInstance();
    }

    /*
     * Retorna os dias inferteis da parceira
     */
    public CalendarDay diasInferteis(CalendarDay ultimoDiaFertil, int cicloObtido, MaterialCalendarView widgetCalendario) {
        ArrayList<CalendarDay> dates = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(ultimoDiaFertil.getDate());
        for (int i = 0; i < cicloObtido - 11; i++) {
            calendar.add(Calendar.DATE, +1);
            CalendarDay diasFerteis = CalendarDay.from(calendar);
            dates.add(diasFerteis);
        }
        widgetCalendario.addDecorator(new EventDecorator(Color.parseColor("#228B22"), dates));

        int ult = dates.size() - 1;
        return dates.get(ult);
    }

    /*
     * Retorna os dias ferteis da parceira
     */
    public CalendarDay diasFerteis(List<CalendarDay> calendarDays, ArrayList<CalendarDay> dates, MaterialCalendarView widgetCalendario) {
        CalendarDay data = calendarDays.get(0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data.getDate());
        for (int i = 0; i < 8; i++) {
            calendar.add(Calendar.DATE, +1);
            CalendarDay diasFerteis = CalendarDay.from(calendar);
            dates.add(diasFerteis);
        }
        widgetCalendario.addDecorator(new EventDecorator(Color.parseColor("#FF8C00"), dates));

        int ultimo = dates.size() - 1;
        return dates.get(ultimo);
    }

    /*
     * Retorna os dias possiveis de TPM(Tensão Pré Menstrual) da parceira;
     */
    public void diasTPM(CalendarDay ultimoDiaInFertil, MaterialCalendarView widgetCalendario) {
        ArrayList<CalendarDay> dates = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(ultimoDiaInFertil.getDate());
        for (int i = 0; i < 2; i++) {
            calendar.add(Calendar.DATE, +1);
            CalendarDay diasFerteis = CalendarDay.from(calendar);
            dates.add(diasFerteis);
        }
        widgetCalendario.addDecorator(new EventDecorator(Color.BLACK, dates));
    }


}
