package app.minervati.keepinmind.com.keepinmind.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.common.AndroidUtil;
import app.minervati.keepinmind.com.keepinmind.common.Constants;

public class DataPilulaFragment extends Fragment {

    private Button btnProximo;
    private Button btnAnterior;
    private FragmentManager fragManager;
    private FragmentTransaction fragmentTransaction;
    private ImageButton telaDadosDois;
    private DatePicker dpDataPilula;
    private Calendar calendar = Calendar.getInstance();
    private Integer dia, mes, ano;

    public DataPilulaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data_pilula, container, false);
        initComponents(view);

        telaDadosDois.setPressed(Boolean.TRUE);

        //RECUPERA O VALOR DA DATA CASO EXISTA SALVA, E ATUALIZA O DATEPICKER.
        String data = (String) AndroidUtil.getMap().get(Constants.DATA);
        atualizaDataToDatePicker(data);

        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction.setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out);
                //MANTEM A DATA GRAVADA
                String dataInicioPilula = pegaValorDataPilula();
                AndroidUtil.setMap(Constants.DATA, dataInicioPilula);

                CicloFragment cicloFragment = new CicloFragment();
                fragmentTransaction.replace(R.id.container, cicloFragment);
                fragmentTransaction.commit();
            }
        });

        btnAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction.setCustomAnimations(R.animator.card_flip_left_in, R.animator.card_flip_left_out,
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out);
                BemVindoFragment bemVindoFragment = new BemVindoFragment();
                fragmentTransaction.replace(R.id.container, bemVindoFragment);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    public void initComponents(View view){
        btnProximo          = (Button) view.findViewById(R.id.btnNext);
        btnAnterior         = (Button) view.findViewById(R.id.btnBack);
        fragManager         = getFragmentManager();
        fragmentTransaction = fragManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        telaDadosDois       = (ImageButton) view.findViewById(R.id.tela_dados_dois);
        dpDataPilula        = (DatePicker) view.findViewById(R.id.dp_dataPilula);
    }

    public String pegaValorDataPilula(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dia = dpDataPilula.getDayOfMonth();
        mes = dpDataPilula.getMonth();
        ano = dpDataPilula.getYear();

        if(!AndroidUtil.isNull(calendar))
            calendar.set(ano, mes, dia);
        else
            calendar = Calendar.getInstance();

        String data = dateFormat.format(calendar.getTime());
        Log.i(Constants.DATA+" GRAVADA", data);

        return data;
    }

    public void atualizaDataToDatePicker(String date){
        String[] dataSalva = obtemDataSalvaInString(date);
        if (dataSalva != null) {
            if (!"".equals(dataSalva[0])){
                dia = Integer.parseInt(dataSalva[0]);
                mes = Integer.parseInt(dataSalva[1])-1;
                ano = Integer.parseInt(dataSalva[2]);
                Log.i(Constants.DATA+" RECUPERADA", String.valueOf(dia+"/"+mes+"/"+ano));
                dpDataPilula.updateDate(ano, mes, dia);
            }
        }
    }

    public String[] obtemDataSalvaInString(String date) {
        String[] dateValues = null;
        if (date != null){
            dateValues = date.split("/");
        }
        return dateValues;
    }
}
