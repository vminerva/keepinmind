package app.minervati.keepinmind.com.keepinmind.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.fragments.BemVindoFragment;

public class BaseActivity extends AppCompatActivity {

    private FragmentManager fragManager;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        initComponents();

        BemVindoFragment bemVindoFragment = new BemVindoFragment();
        fragmentTransaction.replace(R.id.container, bemVindoFragment);
        fragmentTransaction.commit();

    }


    public void initComponents(){
        fragManager         = getFragmentManager();
        fragmentTransaction = fragManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);


    }
}
