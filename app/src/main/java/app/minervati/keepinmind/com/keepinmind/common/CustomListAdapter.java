package app.minervati.keepinmind.com.keepinmind.common;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.minervati.keepinmind.com.keepinmind.R;

/**
 * Created by victor minerva on 28/06/2015.
 */
public class CustomListAdapter extends ArrayAdapter<String> {
    private final Activity context;
    private final List<String> itemname;
    private final List<Integer> imgid;

    public CustomListAdapter(Activity context, List<String> itemname, List<Integer> imgid) {
        super(context, R.layout.listconfig, itemname);

        this.context  = context;
        this.itemname = itemname;
        this.imgid    = imgid;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.listconfig, null, true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.itemName);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        TextView extratxt = (TextView) rowView.findViewById(R.id.itemName);

        txtTitle.setText(itemname.get(position));
        if (!AndroidUtil.isNull(imgid))
            imageView.setImageResource(imgid.get(position));
        extratxt.setText(itemname.get(position));
        return rowView;

    };
}
