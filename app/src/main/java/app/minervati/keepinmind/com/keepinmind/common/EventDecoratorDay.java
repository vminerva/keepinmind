package app.minervati.keepinmind.com.keepinmind.common;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.HashSet;

/**
 * Decorate several days with a dot
 */
public class EventDecoratorDay implements DayViewDecorator {

    private int color;
    private CalendarDay day;
    private HashSet<CalendarDay> dates = new HashSet<>();

    public EventDecoratorDay(int color, CalendarDay day) {
        this.color = color;
        this.day = day;
        dates.add(day);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.addSpan(new DotSpan(6, color));
    }
}