package app.minervati.keepinmind.com.keepinmind.common;

import java.util.HashMap;

/**
 * Created by victor minerva on 31/10/2015.
 */
public class AndroidUtil {

    private static HashMap<Object, Object> map;

    static {
        map = new HashMap<>();
    }


    public static Boolean isNull(Object object) {
        Boolean isNull = false;
        if (object == null)
            isNull = true;

        return isNull;
    }

    public static Boolean isNullOrEmpty(Object object) {
        Boolean isNull = false;
        if (object == null || object.equals(""))
            isNull = true;

        return isNull;
    }

    public static HashMap<Object, Object> getMap() {
        return map;
    }

    public static void setMap(String objOne, Object objTwo) {
        map.put(objOne, objTwo);
    }

    public static String[] obtemDataSalvaInString(String date) {
        String[] dateValues = null;
        if (date != null){
            dateValues = date.split("/");
        }
        return dateValues;
    }
}
