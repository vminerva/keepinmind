package app.minervati.keepinmind.com.keepinmind.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.NumberPicker;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.common.AndroidUtil;
import app.minervati.keepinmind.com.keepinmind.common.Constants;

public class DuracaoMenstruacaoFragment extends Fragment {

    private Button btnProximo;
    private Button btnAnterior;
    private FragmentManager fragManager;
    private FragmentTransaction fragmentTransaction;
    private ImageButton telaDadosQuatro;
    private NumberPicker npMenst;

    public DuracaoMenstruacaoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_duracao_menstruacao, container, false);
        initComponents(view);

        telaDadosQuatro.setPressed(Boolean.TRUE);

        npMenst.setMinValue(1);
        npMenst.setMaxValue(8);

        //PEGA O VALOR DO CICLO CASO EXISTA SALVO
        Integer cicloMenst = (Integer) AndroidUtil.getMap().get(Constants.CICLOMENSTRUAL);
        if(!AndroidUtil.isNull(cicloMenst))
            npMenst.setValue(cicloMenst);

        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction.setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out);
                Integer cicloMenst = npMenst.getValue();
                AndroidUtil.setMap(Constants.CICLOMENSTRUAL, cicloMenst);
                RevisaoFragment revisaoFragment = new RevisaoFragment();
                fragmentTransaction.replace(R.id.container, revisaoFragment);
                fragmentTransaction.commit();
            }
        });

        btnAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction.setCustomAnimations(R.animator.card_flip_left_in, R.animator.card_flip_left_out,
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out);
                Integer cicloMenst = npMenst.getValue();
                AndroidUtil.setMap(Constants.CICLOMENSTRUAL, cicloMenst);
                CicloFragment cicloFragment = new CicloFragment();
                fragmentTransaction.replace(R.id.container, cicloFragment);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    public void initComponents(View view){
        btnProximo          = (Button) view.findViewById(R.id.btnNext);
        btnAnterior         = (Button) view.findViewById(R.id.btnBack);
        fragManager         = getFragmentManager();
        fragmentTransaction = fragManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        telaDadosQuatro     = (ImageButton) view.findViewById(R.id.tela_dados_quatro);
        npMenst = (NumberPicker) view.findViewById(R.id.np_mens_dura);
    }
}
