package app.minervati.keepinmind.com.keepinmind.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.common.AndroidUtil;
import app.minervati.keepinmind.com.keepinmind.common.Constants;

public class EditaDadosInfoFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private EditText inputData, inputCiclo, inputMenstr;
    private TextInputLayout inputLayoutData, inputLayoutCiclo, inputLayoutMenstr;
    private Button btnConcluir, btnCancel;
    private DatePickerDialog datePickerDialog;
    private Calendar calendar;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;


    public EditaDadosInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edita_dados_info, container, false);
        initComponents(view);


        preencheCampos();

        inputData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateDialog().show(getActivity().getFragmentManager(), "DatePicker");
            }
        });

        btnConcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitForm();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    private void preencheCampos() {
        String dataSalva = preferences.getString(Constants.DATA, "");
        if (!AndroidUtil.isNull(dataSalva))
            inputData.setText(dataSalva);

        Integer cicloSalvo = preferences.getInt(Constants.CICLO, 0);
        if (!AndroidUtil.isNull(cicloSalvo))
            inputCiclo.setText(String.valueOf(cicloSalvo));

        Integer cicloMenstrSalvo = preferences.getInt(Constants.CICLOMENSTRUAL, 0);
        if (!AndroidUtil.isNull(cicloMenstrSalvo))
            inputMenstr.setText(String.valueOf(cicloMenstrSalvo));
    }

    public void initComponents(View view) {
        inputLayoutData = (TextInputLayout) view.findViewById(R.id.input_layout_data);
        inputLayoutCiclo = (TextInputLayout) view.findViewById(R.id.input_layout_ciclo);
        inputLayoutMenstr = (TextInputLayout) view.findViewById(R.id.input_layout_menstr);
        inputData = (EditText) view.findViewById(R.id.input_data);
        inputCiclo = (EditText) view.findViewById(R.id.input_ciclo);
        inputMenstr = (EditText) view.findViewById(R.id.input_menstr);
        btnCancel = (Button) view.findViewById(R.id.btnBack);
        btnCancel.setText("CANCELAR");
        btnConcluir = (Button) view.findViewById(R.id.btnNext);
        btnConcluir.setText("CONCLUIR");

        inputData.addTextChangedListener(new MyTextWatcher(inputData));
        inputCiclo.addTextChangedListener(new MyTextWatcher(inputCiclo));
        inputMenstr.addTextChangedListener(new MyTextWatcher(inputMenstr));

        preferences = getActivity().getSharedPreferences(Constants.DADOS, Context.MODE_PRIVATE);
        editor = preferences.edit();

        calendar = Calendar.getInstance();
    }

    private DatePickerDialog dateDialog() {
        Calendar cDefault = Calendar.getInstance();

        datePickerDialog = DatePickerDialog.newInstance(
                this,
                cDefault.get(Calendar.YEAR),
                cDefault.get(Calendar.MONTH),
                cDefault.get(Calendar.DAY_OF_MONTH));

        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        calendar.set(year, monthOfYear, dayOfMonth);

        String valorData = dateFormat.format(calendar.getTime());

        inputData.setText(valorData);
    }

    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateData()) {
            return;
        }

        if (!validateCiclo()) {
            return;
        }

        if (!validateMenstr()) {
            return;
        }

        editor.putString(Constants.DATA, String.valueOf(inputData.getText()));
        editor.putInt(Constants.CICLO, Integer.parseInt(String.valueOf(inputCiclo.getText())));
        editor.putInt(Constants.CICLOMENSTRUAL, Integer.parseInt(String.valueOf(inputMenstr.getText())));
        editor.commit();
        Toast.makeText(getActivity().getApplicationContext(), "Salvo com sucesso!", Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    private boolean validateData() {
        if (inputData.getText().toString().trim().isEmpty()) {
            inputLayoutData.setError(getString(R.string.err_msg_name));
            requestFocus(inputData);
            return false;
        } else {
            inputLayoutData.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validateCiclo() {
        if (inputCiclo.getText().toString().trim().isEmpty()) {
            inputLayoutCiclo.setError(getString(R.string.err_msg_email));
            requestFocus(inputCiclo);
            return false;
        } else {
            inputLayoutCiclo.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMenstr() {
        if (inputMenstr.getText().toString().trim().isEmpty()) {
            inputLayoutMenstr.setError(getString(R.string.err_msg_password));
            requestFocus(inputMenstr);
            return false;
        } else {
            inputLayoutMenstr.setErrorEnabled(false);
        }

        return true;
    }

    /*
    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    */

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_data:
                    validateData();
                    break;
                case R.id.input_ciclo:
                    validateCiclo();
                    break;
                case R.id.input_menstr:
                    validateMenstr();
                    break;
            }
        }
    }
}
