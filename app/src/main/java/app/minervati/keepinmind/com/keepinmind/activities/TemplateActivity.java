package app.minervati.keepinmind.com.keepinmind.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import app.minervati.keepinmind.com.keepinmind.fragments.AjudaFragment;
import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.common.Constants;
import app.minervati.keepinmind.com.keepinmind.fragments.EditaDadosInfoFragment;

public class TemplateActivity extends AppCompatActivity {

    private FragmentManager fragManager;
    private FragmentTransaction fragmentTransaction;
    private Integer posicao;
    private Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template);
        initComponent();


        posicao = extras.getInt(Constants.POSICAO);
        Log.i("POSIÇÃO", String.valueOf(posicao));

        switch (posicao){
            case 0:
                AjudaFragment helpFragment = new AjudaFragment();
                fragmentTransaction.replace(R.id.container_template, helpFragment);
                fragmentTransaction.commit();
                break;
            case 1:
                EditaDadosInfoFragment editFragment = new EditaDadosInfoFragment();
                fragmentTransaction.replace(R.id.container_template, editFragment);
                fragmentTransaction.commit();
                break;
            case 2:
                break;
        }



    }

    public void initComponent() {
        fragManager = getFragmentManager();
        fragmentTransaction = fragManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        extras = getIntent().getExtras();
    }

}
