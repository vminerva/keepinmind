package app.minervati.keepinmind.com.keepinmind.fragments;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.activities.TemplateActivity;
import app.minervati.keepinmind.com.keepinmind.common.Constants;
import app.minervati.keepinmind.com.keepinmind.common.CustomListAdapter;

public class ListItensConfigFragment extends Fragment {

    private ListView listaItensConfig;
    private List<String> itens = new ArrayList<String>();
    private List<Integer> images = new ArrayList<Integer>();
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private FragmentManager fragManager;
    private FragmentTransaction fragmentTransaction;

    {
        itens.add("Ajuda");
        itens.add("Definições");
        itens.add("Notificações");

        images.add(R.mipmap.ic_help);
        images.add(R.mipmap.ic_setting);
        images.add(R.mipmap.ic_ring);
    }

    public ListItensConfigFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_itens_config, container, false);
        initComponent(view);

        CustomListAdapter adapter = new CustomListAdapter(getActivity(), itens, images);

        listaItensConfig.setAdapter(adapter);

        listaItensConfig.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        Intent i_help = new Intent(getActivity(), TemplateActivity.class);
                        i_help.putExtra(Constants.POSICAO, position);
                        //startActivity(i_help);
                        Toast.makeText(getActivity(), "EM BREVE", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Intent i_def = new Intent(getActivity(), TemplateActivity.class);
                        i_def.putExtra(Constants.POSICAO, position);
                        startActivity(i_def);
                        break;
                    case 2:
                        Intent i_not = new Intent(getActivity(), TemplateActivity.class);
                        i_not.putExtra(Constants.POSICAO, position);
                        Toast.makeText(getActivity(), "EM BREVE", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        return view;
    }

    private void initComponent(View view){
        listaItensConfig    = (ListView) view.findViewById(R.id.listItens);
        preferences         = getActivity().getSharedPreferences(Constants.DADOS, Context.MODE_PRIVATE);
        editor              = preferences.edit();
        fragManager         = getFragmentManager();
        fragmentTransaction = fragManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
    }

}
