package app.minervati.keepinmind.com.keepinmind.common;

/**
 * Created by victor minerva on 29/10/2015.
 */
public class Constants {

    public static final String DADOS = "DADOS";
    public static final String DATA = "DATA";
    public static final String CICLO = "CICLO";
    public static final String CICLOMENSTRUAL = "CICLOMENSTRUAL";
    public static final String CELULAR = "CELULAR";
    public static final String HORARIO = "HORARIO";
    public static final String NOMECONTATO = "NOMECONTATO";
    public static final String NIVERNAMORO = "NIVERNAMORO";
    public static final String NIVERNAMORADA = "NIVERNAMORADA";
    public static final int PICK_CONTACT_REQUEST = 1;
    public static final String INFO = "INFO";
    public static final String ERRO = "ERRO";
    public static final String WARN = "WARN";
    public static final String IS_EDIT = "IS_EDIT";
    public static final String HORARIO_ATIVO = "HORARIO_ATIVO";
    public static final String ALARM_SERVICE = "ALARM_SERVICE";
    public static final String POSICAO = "POSICAO";
    public static final String CALENDARIO = "CALENDARIO";
    public static final String DATA_INICIO_COMP = "DATA_INICIO_COMP";
    public static final String DATA_FIM_COMP = "DATA_FIM_COMP";
    public static final String DATA_PROX_COMP = "DATA_PROX_COMP";
}
