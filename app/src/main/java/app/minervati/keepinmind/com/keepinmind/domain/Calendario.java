package app.minervati.keepinmind.com.keepinmind.domain;

import java.util.Calendar;

/**
 * Created by victor minerva on 04/07/2015.
 */
public class Calendario {

    private Calendar dataMenstruacao;

    public Calendario(){}

    public Calendario(Calendar dataMenstruacao){
        this.dataMenstruacao = dataMenstruacao;
    }

    public Calendar getDataMenstruacao() {
        return dataMenstruacao;
    }

    public void setDataMenstruacao(Calendar dataMenstruacao) {
        this.dataMenstruacao = dataMenstruacao;
    }
}
