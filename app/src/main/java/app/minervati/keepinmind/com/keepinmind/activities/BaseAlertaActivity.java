package app.minervati.keepinmind.com.keepinmind.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import app.minervati.keepinmind.com.keepinmind.R;

public class BaseAlertaActivity extends AppCompatActivity {

    private Button btnConcordo;
    private Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_alerta);
        initComponents();

        btnConcordo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public  void initComponents(){
        btnConcordo = (Button) findViewById(R.id.btnNext);
        btnConcordo.setText("Está certo!");
        btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setVisibility(View.GONE);
    }
}
