package app.minervati.keepinmind.com.keepinmind.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import app.minervati.keepinmind.com.keepinmind.R;
import app.minervati.keepinmind.com.keepinmind.business.NotifyReceiver;
import app.minervati.keepinmind.com.keepinmind.common.AndroidUtil;
import app.minervati.keepinmind.com.keepinmind.common.Constants;
import app.minervati.keepinmind.com.keepinmind.common.ViewPagerAdapter;
import app.minervati.keepinmind.com.keepinmind.fragments.OneFragment;
import app.minervati.keepinmind.com.keepinmind.fragments.TwoFragment;

/**
 * @author Viictor Minerva - Team MinervaTI
 * @since 2015
 */
public class HomeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_tab_definicoes,
            R.drawable.ic_tab_calendario
    };
    private ProgressDialog dialog;

    private SharedPreferences preferences;
    private String horario;
    private Integer hora;
    private Integer minuto;
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initComponents();

        dialog.setTitle("Aguarde Por favor...");
        dialog.setIcon(R.mipmap.ic_app);
        dialog.setMessage("CARREGANDO DADOS");
        dialog.setCancelable(false);
        dialog.setInverseBackgroundForced(true);
        dialog.show();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false); //false p/ quando nao desejares que apareça a seta "back".

        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
            }
        }, 7000);

        startAlertaDiaria();

    }

    private void startAlertaDiaria() {
        Calendar calendar = Calendar.getInstance();
        horario = preferences.getString(Constants.HORARIO, "");
        boolean isHoraAtiva = preferences.getBoolean(Constants.HORARIO_ATIVO, false);

        Log.i(Constants.HORARIO_ATIVO, String.valueOf(isHoraAtiva+" - "+horario));

        if (!AndroidUtil.isNullOrEmpty(horario) && isHoraAtiva) {
            Log.i(Constants.HORARIO, "ALERTA ATIVO");
            String[] hourMinute = horario.split(":");
            hora = Integer.parseInt(hourMinute[0]);
            minuto = Integer.parseInt(hourMinute[1]);

            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hourMinute[0]));
            calendar.set(Calendar.MINUTE, Integer.parseInt(hourMinute[1]));
            calendar.set(Calendar.SECOND, 00);

            if ( Calendar.getInstance().getTime().after(calendar.getTime())){
                Log.i(Constants.DATA, "Data corrente é maior que a data informada.");
                calendar.add(Calendar.DAY_OF_MONTH, 1);
            }
            Intent intent1 = new Intent(HomeActivity.this, NotifyReceiver.class);
            Log.i(Constants.DATA, new SimpleDateFormat("dd/MM/yyyy").format(calendar.getTime()));

            PendingIntent pendingIntent = PendingIntent.getBroadcast(HomeActivity.this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager am = (AlarmManager) HomeActivity.this.getSystemService(HomeActivity.this.ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

    public void initComponents() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        dialog = new ProgressDialog(this);
        preferences = getSharedPreferences(Constants.DADOS, Context.MODE_PRIVATE);
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new OneFragment(), "ONE");
        adapter.addFrag(new TwoFragment(), "TWO");
        viewPager.setAdapter(adapter);
    }

    /*
    public void openDialogFragment(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    }
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsBaseActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}