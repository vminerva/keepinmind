package app.minervati.keepinmind.com.keepinmind.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import app.minervati.keepinmind.com.keepinmind.R;

public class BemVindoFragment extends Fragment {

    private Button btnConcordo;
    private FragmentManager fragManager;
    private FragmentTransaction fragmentTransaction;
    private ImageButton telaDadosUm;

    public BemVindoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bem_vindo, container, false);
        initComponents(view);

        telaDadosUm.setPressed(Boolean.TRUE);

        btnConcordo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragmentTransaction.setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out);
                fragmentTransaction.replace(R.id.container, new DataPilulaFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    public void initComponents(View view){
        btnConcordo         = (Button) view.findViewById(R.id.btnConcordo);
        fragManager         = getFragmentManager();
        fragmentTransaction = fragManager.beginTransaction();
        telaDadosUm         = (ImageButton) view.findViewById(R.id.tela_dados_um);
    }
}
